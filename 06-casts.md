# Casts

Every variable has a fixed type that cannot 
be altered afterwards. Nevertheless, there 
are cases where a variable should be interpreted
as something else. 

You might skip this chapter and come back after
reading about protocols and managed code.

## Upcast

Casting an Object to a broader protocol.
Upcasts are implicitly done.

`Person` is an implementation that obeys
to the `TaxPayer` protocol:

```n
Person p = Person("John Doe")
TaxPayer tp = p # implicit upcast
```

## Downcast

Casting an Object to a narrower protocol.
Downcasts must be done explicitly. A downcast
might fail (if the instance cannot be 
downcasted to the given protocol, e.g. if it
does not obeys to it).

`Person` is an implementation that obeys
to the `TaxPayer` protocol:

```n
TaxPayer tp = Person("John Doe")
if let Person p = tp {
    print("TaxPayer is a person")
} else {
    print("TaxPayer is not a person")
}
```

## Forced downcast

Forced downasts should be avoided. Forced
downasts can be used to cast incompatible
data types, e.g. cast `int` to `float`, or 
to do downcasts where you are sure that this 
downcast is possible. Otherwise, the program
is likely to crash.

```n
TaxPayer tp = Person("John Doe")
let Person p = tp # forced downcast which might crash
let int i = 1.5 # interpret the IEEE 1.5 as a two's complement int
```

You might want to use the `int` and `float` converters
for converting these values.

## Optional unwrapping

To use optionals safely, it is required to 
unwrap them and cast them to non-optionals.

```n
Person? p = nil
if let Person q = p {
    print(q.name)
}
```

```n
Person? p = nil
guard let Person q = p else {
    q = Person("John Doe")
}
print(q.name)
```

## Unmanaged pointer

If you use unmanaged code (e.g. bridged C code),
you might use unmanaged pointers to instances.
To optain these pointers, you need to cast these
instances to pointers and vice versa.

```n
Person p = Person("John Doe")
let ptr pt = p # cast to pointer
ptr qt = unmanaged_function(pt)
let Person q = qt
```

