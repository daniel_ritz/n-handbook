# Operators

n uses the following operators. Precedence is from top to bottom:

|    | name |
| -- | ---- |
| `++` | Post-increment   |
| `--` | Post-decrement   |
| `.`  | Dereference      |
| `::` | Member Dereference   |
| `?`  | Null Guard       |
| `++` | Pre-increment    |
| `--` | Pre-decrement    |
| `-`  | Negation         |
| `!`  | Not              |
| `~`  | Complement       |
| `...`| Spread           |
| `..<`| Spread less than |
| `<..`| Spread more than |
| `*`  | Multiplication   |
| `/`  | Division         |
| `%`  | Remainder/Modulo |
| `+`  | Addition         |
| `-`  | Subtraction      |
| `<<` | Left shift       |
| `>>` | Right shift      |
| `<`  | Less than        |
| `<=` | Less or equals   |
| `>`  | More than        |
| `>=` | More or equals   |
| `==` | Equals           |
| `!=` | Not Equals       |
| `&`  | Binary and       |
| `^`  | Binary xor       |
| `|`  | Binary or        |
| `&&` | Logical and      |
| `||` | Logical or       |
| `??` | Null-coalesing   |
| `=`  | Assignment       |
| `+=` | Assignment (with `+, -, *, /, %, <<, >>, &, ^, |`) |


## Dereference

You can access properties, methods or sub-structures 
using the dereference operator

```n
Person p = Person()
p.name
p.rename(to: "John")
```

## Member dereference

Since properties and members usually share the same
name, you can access members using the member dereference 
operator

```n
Person p = Person()
p::name # is not the same a p.name!
```

## Null guard

To check whether a variable is nil, you can use the 
null guard operator:

```n
Person? p = nil
bool isNil = p? // true
```

## Null coalesing operator

You use the null coalesing operator to assign a 
default value when a variable is nil:

```n
Person? p = nil
Person q = p ?? Person("John Doe")
```

## Spread Operator

The spread operator creates an int array for all
values between the given values:

```n
Array<int> arr = 1...5 // {1, 2, 3, 4, 5}
1..<5 // {1, 2, 3, 4}
1<..5 // {2, 3, 4, 5}
```
