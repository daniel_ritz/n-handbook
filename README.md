# Handbook for the n Programming language

This is the handbook for writing programs in n.
This handbook is a guide for writing n programs, as well
as a reference for implementing n compilers

## Chapters

1. [Overview](01-overview.md)
2. Structured Programming

    2. [Variables & Constants](02-variables-constants.md)
    3. [Functions](03-functions.md)
    4. [Operators](04-operators.md)
    5. [Conditionals](05-conditionals.md)
    6. [Casts](06-casts.md)
    7. [Loops](07-loops.md) 
    8. [Exception handling](08-exception-handling.md)
    9. [IO operations](09-io-operations.md)

3. Protocol-Oriented Programming

    10. [Protocols, Implementations, Extensions](10-protocols-implementations-extensions.md)
    11. [Constructors, Destructors](11-constructors-destructors.md)
    12. [Methods](12-methods.md)
    13. [Properties](13-properties.md)
    14. [Protocols](14-protocols.md)
    15. [Extensions](15-extensions.md)
    16. [Reference Counting](16-reference-counting.md)
    17. [Internal Representation](17-internals-structures.md)
    
4. Generic Programming

    18. [Generics](18-generics.md)
    19. [Generic Functions](19-generic-functions.md)
    20. [Generic Structures](20-generic-structures.md)
    21. [Internal Representation](21-internals-generics.md)

5. Aspect-Oriented Programming

    22. [Method Hooks](22-method-hooks.md)

6. Modular Programming

    23. [Namespaces](23-namespaces.md)
    24. [Internal Representation](24-internals-namespaces.md)

7. Concurrent Programming

    25. [Threads](25-threads.md)
    26. [Synchronization](26-synchronization.md)
    27. [Threading Primitives](27-threading-primitives.md)

8. C Bridge

    28. [C Functions](28-c-functions.md)
    29. [Name Mangling](29-name-mangling.md)

9. Standard Library

