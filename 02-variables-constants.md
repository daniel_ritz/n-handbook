# 02. Variables and Constants

## Scalar Types

n supports the following scalar types:

| type  | info                                                  |
| ----- | ----------------------------------------------------- |
| int   | integer data with at least 32-Bit                     |
| float | floating-point data, 64-Bit (like C's double)         | 
| bool  | boolean values, at least 1-Bit                        |
| str   | internal strings (are handled like they were scalars) |
| ptr   | pointer (like C's byte pointer)                       |
| void  | no type; used if a return type is required but nothing should be returned |

Scalar types have default values if no value was assigned
at declaration:

| type  | default value |
| ----- | ------------- |
| int   | 0             |
| float | 0.0           |
| bool  | false         |
| str   | ""            |
| ptr   | NULL          |

## Variable Declaration

Variables can be declared in global or in a closure's scope.
Every Variable must be declared with a type and can be initialized
with a value; however, scalar types do not need to be initialized.
Variables MUST NOT be used before their declaration, though, a 
compiler MIGHT hoist a variable to the beginning of a scope.

Variable names must start with a letter and can contain letters 
or digits.

```n
int maximum = 5 # global variable

main: () -> void {
    int counteri # local variable in `main` scope with default value
    
    { # anonymous closures create a new scope
        int i = 1 # variable local to this scope
        int counter = 5 # local variables can hide lower-scope variables
    }

    # counter is still 0
    # i is not accessible from here
    maximum++
    
}
# maximum is now 6
```


## Tuples

Tuples are fixed-size compunds of multiple values of same or different
types. Tuples are value types, that means, they are copied when passing
around. Tuples must be initialized upon declaration.

```n
(int, int) tup = (1, 2) # 2-tuple of two integers
(float, bool, str) tup2 = (1.0, false, "Hi") # 3-tuple
```

## Arrays

Arrays are variable sized collections of values of the same type.
Arrays are initialized to an empty list upon declaration,
however, it is recommended to specify their initial value.
Arrays need to be associated with a specific type. This associated
type cannot be changed afterwards.

There are many ways to initialize an array:
* Empty array with a minimum size: `Array(5)` creates an Array of size 5
* Specific values: `{1, 2, 3, 4, 5}` creates an Array with these values
* Spread Range values: `1...5` creates an array with integer values 1 to 5
* Value function: `{(i := 1..5) { return i * 2}}` creates an array with 
    values 2, 4, 6, 8, 10

```n
Array<int> empty = Array<int>(5) # empty array with size 5
Array<int> empty2 = Array(5) # shorthand dropping associated type on initializer
Array<int> fib = {1, 1, 2, 3, 5, 8} # specific values
Array<bool> toggle = {(i := 1...5) { return i % 2 == 0 }}
    # array with values {false, true, false, true, false}
Array<int> range = 1...5 # aray with values {1, 2, 3, 4, 5}
```

## Protocol and Implementation types

Variables with protocol or implementation types are called instances.
An instances can be optained by calling an implementation's constructor
(that is it's Type name). Protocols cannot be instanciated. 
See the corresponding chapter for more info.

```n
Person p = Person("John Doe")
TaxPayer t = Person("Jane Doe") # since Person is a TaxPayer
```

## Assigning values

Variables can be assigned values at declaration or afterwards.
Assigned value can be from variables, constants, expression results
or function call results.

```n
int i = 2 # assigning a constant at declaration
i = 3 # assigning a constant
i = i + 2 # assigning an expression's result
i = max(i, 0) # assigning a function call's result
```

## Optionals

It is sometimes neccessary for variables to have an error- or invalid
state. This is traditionally done by using magic values, such as (-1).
n introduces Optional values for this purpose: An optional variable can 
have any value defined by it's type, as well as an invalid state called 
`nil`. You can think of an optional `int` as a tuple `(bool, int)` 
whereas the first entry specifies whether the variable is `nil` or
valid, and the second entry specifies its value in the valid case.

Additionally, traditional languages allow an instance variable to 
reference an instance or to be a reference to the NULL object. 
In n, every non-optional variable is required to hold an instance
other than nil. Optional varaibles are the only way to allow nil
references.

Optional types are specified with a `?` after their type name.

```n
int i = nil # Error: This variable is not allowed to become nil
int? j = nil # This variable can become nil
Person p = nil # Error: cannot become nil
Person? q = nil # This variable can become nil
(int, float)? tup = nil
(int, int)? -> int func = nil # optional function; mind the ? placement
```
