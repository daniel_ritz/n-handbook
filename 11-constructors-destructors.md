# Constructors, Destructors

# Constructor

Every non-optional member of an Implementation
needs to be initialized upon creation of an
Instance. The easiest approach is to define
default values for non-optional members:

```n
Person: {
    str name = ""
    int age = 0
    Person? partner     # optional member, will be initialized with nil
}
```

This Implementation can now be instantiated:

```n
Person p = Person()
```


Initializing a Person with a default name and age 
is not really satisfying: A Person should have
a name right from the start, in order to avoid
later renaming or ambiguities. Therefore, n allows
constructors to set every by-then non-initialized member.

```n
Person: {
    str name
    int age
    Person? partner

    init: (str name) -> void {
        self::name = name
        self::age = 0
    }
}
```

```n
Person p = Person("John Doe")
```

A constructor is any method named `init`. It's return 
type is ignored and should be `void`. It is possible
to overload constructors and to use them each as 
appropriate.

```n
Person: {
    str name
    int age
    
    init: () -> void {
        self::name = ""
        self::age = 0
    }

    init: (str name) -> void {
        self::name = name
        self::age = 0
    }

    init: (str name, int aged age) -> void {
        self::name = name
        self::age age
    }
}
```

```n
Person p = Person()
Person q = Person("John Doe")
Person r = Person("John Doe", aged: 33)
```


## Destructors

It might be important to do some work when an
Instance is destroyed (e.g. close a File, write 
a log, etc). Those actions are done in a method
with signature `deinit: () -> void`. It is not
possible to have multiple destructors. 
Destructors should be avoided if possible.
    

