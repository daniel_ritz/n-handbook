# Conditionals

## If Statement

n supports the traditional if statement. n does
not require braces around the condition, but 
requires curly braces around the branch-body.
Compound statements using logical and/or
use short validation.

```n
if i < 5 {
    print("i is less than 5")
} else if i == 5 {
    print("i is 5")
} else {
    print("i is more than 5")
}
```

## Switch

IMPORTANT: The switch syntax is likely to 
change in future versions

When checking for a lot of cases, the 
if statement can be replaced by a switch
statement for clarity. Switch accepts a
variable and holds branches for any
value that should be handled. An additional
`default` branch is used for unmatched 
values.

```n
switch i {
    case 1 {
        print("Is 1")
    }
    case 2 {
        print("Is 2")
    }
    case default {
        print("Default " + str(i))
    }
}
```

## Guard

Guard statements are used to check whether a
variable's value matches a required condition,
and to handle an invalid case. This can involve 

* Bailing out by throwing an exception
* Bailing out by returning an invalid state
* Fixing the problem by assigning a default value

```n
guard i > 0 else {
    i = 0 // fix invalid i
}
```

Guard statements should only be used for such cases
as intended. Use if statements otherwise.


