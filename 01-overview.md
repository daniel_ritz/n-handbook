# Overview

n is a protocol-oriented multi-purpose programming language.
It is suitable for cli applications and might be used for
embedded systems (however, because of its object-overhead,
it might not be the first choice for such).
Most of n's functionality can be reduced to C. Therefore,
this guide will use C samples to explain n's functionality
and internal implementation. The C samples are based on the
reference implementation nlang and might be implemented 
differently in other compilers.
n is based on C's standard library and can used C functions
directly.

## Hello, World!

Let's start in style with a simple Hello World application:

```n
main: () -> int {
    print("Hello, World!")
    return 0
}
```

This sample is not that much different than C:

```C
int main() {
    printf("%s", "Hello, World!")
    return 0
}
```

The key difference is a changed function definition.
The function signature `int main()` becomes `main: () -> int`.

## Building Blocks

n is based around a few Building blocks:
* Functions
* Structures
* Closures
* Keywords
* Tags
* Expressions
* Types
* Variables
* Constants

A function is a collection of commands which are associated
with a name. You can pass arguments to a function and retrieve 
a return value. Functions do not need to be defined in a n file,
but the n compiler needs to know its name and signature. Functions
can be passed around in variables like any other instance in n.

```n
# Function definition
foo: (int a, int b) -> int

# Function declaration
foo: (int a, int b) -> int {
    return a + b
}
```

A structure is a collection of data and it's associated functions.
A structure can be a protocol, an implementation or an extension.
(more on this in the corresponding section)

```n
# Implementation Structure
bar: {
    int a

    foo: (int b) -> int {
        return self::a + b
    }
}
```

A closure is a block of code which is usually associated with a
function's body or a control structure. Nevertheless, a closure
can stand alone inside another closure and can be used to group 
instructions. Closures start and end with curly braces.

```n
foo: () -> void { # start of closure which is used as body for foo

    int i = 0
    if 1 < 2 { # Start of closure
        i++
    } # End of closure

    { # Start of stand-alone closure
        i++
    } # End of stand-alone closure
}            
```

Keywords are used for flow control and cannot be used for variable
or structure names. Examples for keywords are: `if`, `else`, `guard`.

Tags alter the meaning of a following structure, function, closure
or expression. Tags start with `@` and can be assigned a value.

```n
@obeys: Person      # Tag `obeys` with value `Person`
@obeys: TaxPayer    # Tags with the same name can be applied multiple times
@protocol Student: {} # Tags without value can be inline
```

Expressions are any arithmetic or logical etc expressions in
a program, e.g. `i + 4`, `i < 3 && i > 1`.

A Type defines which values a variable can hold and how they are 
structured. Types define the behavior of variables as well. 
n differentiates between scalar types, structure types, tuples,
arrays and function types. Each type can be declared as optional,
because non-optional types cannot be nil.

```n
int i # variable i of type int
Array<int> arr # variable arr of type Array which holds int values
Person p # variable p of type Person
Person? q # variable q of type optional person, which can be nil
```

Variables are places in memory which can be assigned a value.
Constants are constant values which cannot be altered

```
int i = 1 # variable i, assigned constant 1
i = 2 # Variables can be changed. i is assigned constant 2
str hello = "Hello" # variable hello which is assigned a 
                    # constant string
```

## Comments

Comments can either be line comments starting with `#` or `//` 
until the end of line, or multi-line comments starting
with `/*` and ending with `*/`. Comments do not influence 
program semantics.


