# Functions

Functions are traditionally a collection of instructions 
which can be invoked from multiple program-parts. 
Functions are the primary means of structuring program
code. In n, functions become near first class citizens: 
they can be used almost the same way as other constants.

## Function Declaration

A function declaration tells the compiler the signature
of an existing function. This function does not necessarily 
need to be defined in the same file. Usually, function 
declaration and definition are done together.

A function's signature exists of its parameters and return
type. Parameters have a type and an internal name, which
is used inside the function. Additionall, parameters can
have external names which must be specified every time 
the function is called (so-called named parameters). 
External paramter names are not required.


```n
# Function with unnamed parameters and no return type
foo: (int x, str t) -> void
# Function with external parameter names and int return type
add: (int x, int and y) -> int
```

## Function Definition

A function definition specifies a function's body. 
Function definitions consist of a function declaration 
and a closure. Inside this closure, the paramters can
be accessed with their internal names.

A function's return type can be any type and can optionally
assigned a variable's name. When a function defines a 
return value's variable name, this function MUST NOT have
more than one exit point.

A function can return a value by using the `return` keyword.
Functions with `void` return type MUST NOT pass a value to
the return statement. There can be multiple `return` statements
inside a function's body; each of them is called an exit point.

```n
# Simple function definition
foo: (int x, str t) -> void {
    # do some stuff
    int y = x + 1
    str b = t + " World!"
}

# Function with return value
add: (int x, int and y) -> int {
    return x + y
}

# Function using a return name
add: (int x, int and y) -> int z {
    z = x + y
    # return z is automatically appended
}
```

## Calling functions

A function is called by passing arguments for each
parameter. Named parameters must be specified with their
external name upon calling.

```n
str hello = "Hello"
foo(1, hello)
int z = add(1, and: 3) // returns 4
```

## Function Types

Functions can be passed around like other variables

```
foo: (int x) -> int {
    return x + 1
}

foo2: (int x) -> int {
    return x * 3
}

bar: ((int) -> int func) -> int {
    return func(1)
}

main: () -> void {
    bar(foo) // returns 1 + 1 = 2
    bar(foo2) // return 1 * 3 = 3
}
```

## Lambda functions

Functions can be defined inline, as so-called lambdas
or anonymous functions. This is usually used when
a function should be passed as a parameter. There are
two styles for defining lambdas:

```n
# bar accepts a function as parameter
bar: ((int) -> int func) -> int

# pass a lambda function to bar
bar((int x) -> int {
    return x * 2
})

# pass a single-expression lambda function to bar
bar((int x) -> int x * 2)
```

The latter example defines the single expression
for the lambda function inline without the `return`
statement and curly braces.


## Function Overloading

It is possible to have multiple functions with the 
same name, but different parameter types. It is
not possible to have the same name and parameter types
but a different return type.

```n
add: (int x, int y) -> int
add: (float x, float y) -> float
add: (float x, float y) -> int # not possible!
```

# ndoc

Like javadoc or phpdoc, ndoc specifies how code
can be marked up with additional information.
ndoc uses the extensible tag system. Since tags can
have semantic meaning in n, this enables parameter
validation without cluttering the code.

Tags allowed on functions:
```n
@param: NAME DESCRIPTION
@param: NAME (CONDITION) DESCRIPTION
@param: NAME (RANGE) DESCRIPTION
@returns: DESCRIPTION
```

This can be used as such:
```
@param: x (0...5)   The x value
@param: y (y > 0)   The y value
@returns: x/y
foo: (int x, int y) -> int {
    return x / y
}

# this is expanded to:
foo: (int x, int y) -> int {
    guard x >= 0 && x <= 5 else {
        throw InvalidArgumentException()
    }
    guard y > 0 else {
        throw InvalidArgumentException()
    }
    return x / y
}
```

