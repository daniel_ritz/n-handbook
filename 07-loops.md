# Loops

n knows `while`, `do-while` and `for` loops.

## While

Most programs use recurring code that 
should be looped until a condition is met.
n provides simple while loops, which
execute their body closure while their
condition is met

```n
int x = 1
int y = 1
while y < 100 {
    int a = x + y
    x = y
    y = a
    print(y)
}
```

## Do-While

Some loops are required to be invoked at
least once, and their condition checked 
upon attempting to exit.

```n
int i = 0
do {
    print(i)
    ++i
while i < 5
```


## For

For loops are recommended when looping
over a list of values, e.g. a data range,
array or list. Every iterable collection
can be looped over

```n
# looping over a range of values
for int i := 1...5 {
    print(i)
}

# looping over an iterable
Array<int> arr
for int value := arr {
    print(value)
}

# looping over custom sequence
for float f := {(i := 1...5) { return i / 2 }} {
    print(f)
}
```

## Break and Continue

Sometimes, it is required to break out of
a loop or to continue with the next iteration
without executing the remaining part.
These cases can be achieved by using the
`break` or `continue` statements.

```n
int j = 2
for int i := 1...5 {

    if i % 2 == 0 { # skip even numbers
        continue
    }

    --j
    if j <= 0 {
        break   # break out, because j
                # must not become negative
    }
}
```


