# Exception handling

n has two approaches to exception handling:
* Optionals
* Catch-Blocks
It is usually recommended to use optionals.

## Exception handling with optionals

Using optionals, a function can not only
return a valid value, but an error value
as well. C used to return (-1) for errors,
which is the same approach.

```n
readLine: () -> str? {
    str line = ""
    while let str char = getChar() {
        if char == "\n" {
            return line
        } else {
            line += char
        }
    }
    return nil # end of stream but no "\n"
}

main: () -> void {

    // check for optional value
    guard let str line = readLine() else {
        print("No input")
    }
    
    print("Read line: " + line)

}
```

## Catch-Block

A function can throw exceptions. An exception
is an Instance of the `Exception` protocol.
Exceptions can be caught and handled with 
corresponding catch blocks, which can be appended
to any closure.

```n
@throws: FileSystemException
openFile: (str name) -> File {
    # do some magic here
    throw FileSystemException(message: "File not found", file: name)
}

func1: () -> void {
    File f = openFile("/invalid")
} catch: FileSystemException ex {
    # trailing catch block
    print("File " + ex.name + " not found")
} catch: Exception ex {
    # trailing catch block for any other exception
    print(ex.message)
}

func2: () -> void {

    {
        File f = openFile("/invalid")
    } catch: FileSystemException ex {
        # catch block for specific sub-closure
        print("File " + ex.file + " not found")
    }

}
```

