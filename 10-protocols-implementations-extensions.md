# Protocols, Implementations, Extensions

Protocols specify the known public interface
of a type. Therefore, every type obeys to at
least one protocol. Since protocols only 
specify the interface, the functionality 
must be implementet in so-called Implementations.
Those Implementations are specific 
implementations for a functionality which
can differ for each Implementation.
A protocol can be implemented by different
Implementations.
To allow extensibility of built-in types 
or to upgrade an Implementation with
non-core functionality or to provide
default implementations for Protocols,
Extensions can be used.

Protocols can be compared to Interfaces or
Abstract Classes. Implementations are 
mostly equivalent to Classes. Extensions
might be compared to Mixins or Traits.

A Structure (or struct) in n is the
generic term for Protocols, Implementations
and Extensions.

Structures consist of data variables 
(called `members`) and functions (called
`methods`). A publicly accessible `member`
is called a `property`. `Computed properties`
are `properties`, whose values are dynamically 
generated upon access and do not have a
directly related `member`.

Protocols and Extensions do not have `members`.

|               | methods | members | properties | computed properties |
| ------------- | ------- | ------- | ---------- | ------------------- |
| Protocol      | yes     | no      | yes        | yes                 |
| Implementation| yes     | yes     | yes        | yes                 |
| Extension     | yes     | no      | no         | yes                 |

# Member vs Property

A `member` is a variable which is assigned to 
a specific instance of a Structure. A member
should not be accessed from outside of the 
structure (think of it as a private variable).
Properties are public representations of data,
whose underlying data is usually stored in 
a property. A property's value is accessed
through getter and setter methods. Those 
getters and setters can validate the data 
before storing it. Usually, members and 
properties use the same name. It is possible
to expose properties which rely on a 
computation of other properties (e.g. a
`count` property which relays the `count`
of a list stored as a member). Those 
properties use a getter which computes the
value, but do not use a underlying member
of the same name. Those properties are
called `computed properties`.

## Protocol

A protocol consists of a declaration of
properties and a declaration of methods.
Protocols do not specify whether a 
property uses a member or a property
is a computed property.

```n
@protocol Person: {
    @get @set str name          # r/w property `name`
    @get @set Date birthdate    # r/w property `birthdate`
    @get int age                # readonly property `age`
                                # this property CAN be implemented
                                # as r/w, but it does not need to

    greet: (Person other) -> void   # method
}
```

## Implementation

An Implementation consists of a definition 
of members and a definition of properties 
and mehods.

```n
Person: {
    @get @set str name          # this creates the member `name` AND
                                # the getters and setters
    Date birthdate              # this only creates the member `birthdate`
    
    init: (str name, Date birthdate) -> void {      # Constructor
        self::name = name                           # Set member `name`
        self::birthdate = birthdate
    }

    birthdate: (Date value) -> void {               # `birthdate` setter
        guard value > Date.now() else { return }
        self::birthdate = value
    }

    birthdate: () -> Date {                         # `birthdate` getter
        return self::birthdate
    }
    
    age: () -> int {                                # computed property `age` getter
        return (Date.now() - self::birthdate).year
    }

    greet: (Person other) -> void {                 # method
        print(self.name + ": Hello, " + other.name) # access other's property `name`        
    }
}
```

## Extension

A Extension consists of a definition of
computed properties and methods.

```n
@extension Person: {

    knight: () -> bool {                            # Computed property
        return self.name.prefix(3) == "Sir"         # Extensions cannot access members
    }
    
    farewell: (Person other) -> void {              # method
        print(self.name + ": Farewell, " + other.name)
    }
}
```

